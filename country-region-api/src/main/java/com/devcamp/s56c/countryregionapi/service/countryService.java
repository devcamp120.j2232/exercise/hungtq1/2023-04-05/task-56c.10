package com.devcamp.s56c.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.s56c.countryregionapi.model.country;

@Service
public class countryService {
    @Autowired
    private regionService regionService;
    country vietnam = new country("VN", "Vietnam");
    country us = new country("US", "America");
    country russia = new country("RS", "Russia");

    public ArrayList<country> getAllCountries(){
        ArrayList<country> allCountries = new ArrayList<>();
        
        vietnam.setRegions(regionService.getRegionVietnam());
        us.setRegions(regionService.getRegionUS());
        russia.setRegions(regionService.getRegionRussia());

        allCountries.add(vietnam);
        allCountries.add(us);
        allCountries.add(russia);

        return allCountries;
    }
}
