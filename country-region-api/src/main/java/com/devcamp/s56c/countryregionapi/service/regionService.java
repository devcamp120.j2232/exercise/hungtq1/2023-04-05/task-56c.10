package com.devcamp.s56c.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.s56c.countryregionapi.model.region;

@Service
public class regionService {
    region hanoi = new region("HN", "Hanoi");
    region saigon = new region("SG", "Saigon");
    region danang = new region("DN", "DaNang");

    region newyorks = new region("NY", "NewYorks");
    region florida = new region("FLO", "Florida");
    region texas = new region("TX", "Texas");

    region moscow = new region("MS", "Moscow");
    region kaluga = new region("KL", "Kaluga");
    region sainpeterburg = new region("SP", "Saint Peterburg");

    public ArrayList<region> getRegionVietnam(){
        ArrayList<region> regionsVietnam = new ArrayList<>(); 
        regionsVietnam.add(hanoi);
        regionsVietnam.add(saigon);
        regionsVietnam.add(danang);
    
        return regionsVietnam;
    
    };

    public ArrayList<region> getRegionUS(){
        ArrayList<region> regionsUS = new ArrayList<>(); 
        regionsUS.add(newyorks);
        regionsUS.add(florida);
        regionsUS.add(texas);
    
        return regionsUS;
    
    };

    public ArrayList<region> getRegionRussia(){
        ArrayList<region> regionsRussia = new ArrayList<>(); 
        regionsRussia.add(moscow);
        regionsRussia.add(kaluga);
        regionsRussia.add(sainpeterburg);
    
        return regionsRussia;
    
    };
}
