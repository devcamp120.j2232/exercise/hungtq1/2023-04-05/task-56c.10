package com.devcamp.s56c.countryregionapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s56c.countryregionapi.model.country;
import com.devcamp.s56c.countryregionapi.service.countryService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class countryControler {
    @Autowired
    private countryService countriesService;

    @GetMapping("/countries")
    public ArrayList<country> getAllCountries(){
        ArrayList<country> allCountries = countriesService.getAllCountries();

        return allCountries;
    }
}
