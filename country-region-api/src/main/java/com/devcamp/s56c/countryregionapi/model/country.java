package com.devcamp.s56c.countryregionapi.model;

import java.util.ArrayList;

public class country {
    private String countryCode;
    private String countryName;
    private ArrayList<region> regions;
   
    public country() {
    }

    public country(String countryCode, String countryName) {
        this.countryCode = countryCode;
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public ArrayList<region> getRegions() {
        return regions;
    }

    public void setRegions(ArrayList<region> regions) {
        this.regions = regions;
    }

    public country(String countryCode, String countryName, ArrayList<region> regions) {
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.regions = regions;
    }

    public ArrayList<country> getAllCountries() {
        return null;
    }
}
